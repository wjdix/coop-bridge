(ns coop-bridge.game
  (:require [coop-bridge.hand]))

(defn deal []
  (into [] (map #(into #{} %) (partition 13 (shuffle coop-bridge.hand/cards)))))

(defn new-game []
  (let [[north east south west] (deal)]
    {:north (coop-bridge.hand/new-north-hand north)
     :east (coop-bridge.hand/new-east-hand east)
     :south (coop-bridge.hand/new-south-hand south)
     :west (coop-bridge.hand/new-west-hand west)
     :bid-history []
     :to-bid :north}))

(defn play-once [{:keys [to-bid] :as game}]
  (let [active-hand (to-bid game)
        next (to-bid coop-bridge.hand/succ)
        bid (coop-bridge.hand/bid active-hand)]
    (-> game
        (update-in [:bid-history] #(conj % bid))
        (update-in [:to-bid] (fn [_] next))
        (update-in [:north] #(coop-bridge.hand/receive-bid % bid))
        (update-in [:east] #(coop-bridge.hand/receive-bid % bid))
        (update-in [:south] #(coop-bridge.hand/receive-bid % bid))
        (update-in [:west] #(coop-bridge.hand/receive-bid % bid)))))

(defn all-known? [{north :north south :south east :east west :west}]
  (= 51 (:card-counter north)))

(defn agree? [{n :north s :south e :east w :west}]
  (= (:hands n) (:hands s) (:hands e) (:hands w)))

(defn play [game]
  (if (all-known? game)
    game
    (play (play-once game))))

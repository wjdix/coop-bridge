(ns coop-bridge.hand)

(def succ {:north :east
           :east  :south
           :south :west
           :west  :north})

(def suits [:clubs :diamonds :hearts :spades])

(def ranks [2 3 4 5 6 7 8 9 10 :jack :queen :king :ace])

(defn build-suit [suit]
  (map (fn [rank] {:suit suit :rank rank}) ranks))

(def cards
  (flatten (map #(build-suit %) suits)))

(def levels [1 2 3 4 5 6 7 8 9])
(def strains [:clubs :diamonds :hearts :spades :notrump])

(defn build-level [level]
  (map (fn [strain] {:level level :strain strain}) strains))

(def bids
  (flatten (map #(build-level %) levels)))

(def default-hand
  {:seat :north
   :hands {:north #{}
           :east  #{}
           :south #{}
           :west  #{}}
   :card-counter 0
   :bid-history  (list)})

(defn- new-seat-for-hand [seat cards-held]
  (-> default-hand
      (update-in [:seat] (fn [_] seat))
      (update-in [:hands seat] (fn [_] cards-held))))

(def new-north-hand (partial new-seat-for-hand :north))
(def new-east-hand (partial new-seat-for-hand :east))
(def new-south-hand (partial new-seat-for-hand :south))
(def new-west-hand (partial new-seat-for-hand :west))

(def opponents {:north #{:east :west}
                :east  #{:north :south}
                :south #{:east :west}
                :west  #{:north :south}})

(defn next-suit-bid [bid]
  (nth bids
       (inc (.indexOf bids bid))))

(defn bid-over-opponent [bid most-recent-suit-bid]
  (cond
    (= bid :double) :redouble
    (= bid :redouble) (next-suit-bid most-recent-suit-bid)
    :else :double))

(defn bid-over-partner [most-recent-suit-bid]
  (next-suit-bid most-recent-suit-bid))

(defn opponents? [s1 s2]
  (if (nil? s1)
    false
    ((s1 opponents) s2)))

(defn most-recent-bid [history]
  (let [first (first history)
        rest (rest history)]
    (if (= :pass (:bid first))
      (most-recent-bid rest)
      first)))

(defn suit-bid? [bid]
  (not (or
        (= bid :pass)
        (= bid :double)
        (= bid :redouble))))

(defn most-recent-suit-bid [history]
  (let [{bid :bid :as bid-with-seat} (first history)
        rest (rest history)]
    (if (suit-bid? bid)
      bid-with-seat
      (most-recent-suit-bid rest))))

(defn last-two-passes? [history]
  (let [{b :bid} (first history)
        rest (rest history)]
    (if (or (not (= b :pass)) (empty? rest))
      false
      (let [{b :bid} (first rest)]
        (= b :pass)))))

(declare next-bid)

(defn jump-bid [most-recent-bid most-recent-suited-bid current-declarer seat]
  (cond
    (= most-recent-bid :double)
      (-> most-recent-suited-bid next-suit-bid)
    (= most-recent-bid :redouble)
      (-> most-recent-suited-bid next-suit-bid next-suit-bid)
    (opponents? current-declarer seat)
      (next-suit-bid most-recent-suited-bid)
    :else
      (-> most-recent-suited-bid next-suit-bid next-suit-bid)))

(defn jump-bid-from-history [history seat]
  (let [{most-recent-seat :seat most-recent-bid :bid} (most-recent-bid history)
        {most-recent-suit-bid :bid declarer :seat} (most-recent-suit-bid history)]
    (jump-bid most-recent-bid most-recent-suit-bid declarer seat)))

(defn next-bid [history seat]
  (let [{most-recent-seat :seat most-recent-bid :bid} (most-recent-bid history)
        {most-recent-suit-bid :bid declarer :seat} (most-recent-suit-bid history)]
    (if (nil? most-recent-seat)
      {:strain :clubs :level 1}
      (if (last-two-passes? history)
        (jump-bid most-recent-bid most-recent-suit-bid declarer seat)
        (if (opponents? most-recent-seat seat)
          (bid-over-opponent most-recent-bid most-recent-suit-bid)
          (bid-over-partner most-recent-suit-bid))))))


(defn next-pass [history seat]
  (if (last-two-passes? history)
    (next-bid history seat)
    :pass))

(defn bid [hand]
  (let [seat         (:seat hand)
        held         (get-in hand [:hands seat])
        bid-history  (:bid-history hand)
        counter      (:card-counter hand)
        subject-card (nth cards counter)]
    {:seat seat
     :bid (if (held subject-card)
            (next-bid bid-history seat)
            (next-pass bid-history seat))}))

(defn- third-seat-bid? [{h :bid-history} bid]
  (last-two-passes? h))


(defn- receive-from-third-seat
  [{c :card-counter h :bid-history :as hand} {b :bid s :seat :as bid-with-seat}]
  (let [current-card (nth cards c)
        seat-holding-current-card (if (= b (jump-bid-from-history h s)) s (succ s))]
    (-> hand
        (update-in [:hands seat-holding-current-card]
                   #(if (= (:seat hand) seat-holding-current-card)
                      %
                      (conj % current-card)))
        (update-in [:card-counter] inc)
        (update-in [:bid-history] #(conj % bid-with-seat)))))

(defn- receive-from-non-third-seat
  [{:keys [card-counter] :as hand}
   {:keys [bid seat] :as bid-with-seat}]
  (if (= bid :pass)
    (update-in hand [:bid-history] #(conj % bid-with-seat))
    (let [current-card (nth cards card-counter)]
      (-> hand
          (update-in [:hands seat] #(conj % current-card))
          (update-in [:card-counter] inc)
          (update-in [:bid-history] #(conj % bid-with-seat))))))

(defn- receive-from-self [hand {:keys [bid] :as bid-with-seat}]
  (-> hand
      (update-in [:card-counter] (if (= :pass bid) identity inc))
      (update-in [:bid-history] #(conj % bid-with-seat))))

(defn- receive-from-other [hand bid]
  (if (third-seat-bid? hand bid)
    (receive-from-third-seat hand bid)
    (receive-from-non-third-seat hand bid)))

(defn receive-bid [hand {:keys [seat] :as bid}]
  (if (= seat (:seat hand))
    (receive-from-self hand bid)
    (receive-from-other hand bid)))

(defn all-known? [{hands :hands}]
  (let [{n :north e :east s :south w :west} hands]
    (and
     (= 13 (count n))
     (= 13 (count e))
     (= 13 (count s))
     (= 13 (count w)))))
